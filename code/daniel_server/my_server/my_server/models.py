from django.db import models


class RandomValue(models.Model):
    id = models.AutoField(primary_key=True)
    first = models.IntegerField(default=0)
    second = models.IntegerField(default=0)
    resNum = models.IntegerField(default=0)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%d' % self.resNum
