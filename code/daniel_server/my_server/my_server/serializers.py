from rest_framework import serializers
from my_server.models import RandomValue


class RandomValueSerializer(serializers.ModelSerializer):
    class Meta:
        model = RandomValue
        fields = ['id', 'first', 'second', 'resNum', 'creation_date']
