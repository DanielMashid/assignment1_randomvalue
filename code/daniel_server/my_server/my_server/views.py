import random

from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from my_server.serializers import RandomValueSerializer
from my_server.models import RandomValue


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def generate_new_value(request):
    firstNum = float(request.GET.get('firstNumber', ''))
    secondNum = float(request.GET.get('secondNumber', ''))
    randomNumRes = get_random_value(firstNum, secondNum)
    valuesAndRes_obj = RandomValue(first=firstNum, second=secondNum, resNum=randomNumRes)
    valuesAndRes_obj.save()
    serializer = RandomValueSerializer(valuesAndRes_obj)
    return Response(serializer.data, status=status.HTTP_200_OK)


def get_random_value(number1, number2):

    if number1 > number2:
        random_value = random.randint(int(number2), int(number1))
    else:
        random_value = random.randint(int(number1), int(number2))

    return random_value
