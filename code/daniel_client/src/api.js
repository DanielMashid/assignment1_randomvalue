import Axios from 'axios'
import * as url from "url";

const $axios = Axios.create({
    baseURL: '/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})

//Example of a cross-cutting concern - client api error-handling
$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });


class RandomValueService {
    static getRandomValue(number_1, number_2) {
        return $axios
            .get('values/generate-new-value', {params:{firstNumber:number_1,secondNumber:number_2}})
            .then(response => response.data)
    }
}

const service = {
    RandomValueService
}

export default service
