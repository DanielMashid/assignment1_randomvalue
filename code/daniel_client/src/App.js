import './App.css';
import {Component, useState} from "react";
import service from "./api";


function App() {

    const [number1, setNumber1] = useState("");
    const [number2, setNumber2] = useState("");
    const [randNum, setRandNum] = useState("");


    const generateNewValueOnClick = () => {
        if(isNaN(number1) || isNaN(number2)){
            setRandNum('Please Enter Only Numbers !!')
        }
        else{
            service.RandomValueService.getRandomValue(number1, number2)
                .then(response => {
                        setRandNum(response.resNum)
                })
        }

    }


    return (
        <div className={'app'}>
            <center>
                <h1>Hello and welcome to mission number 1</h1>
                <h2>Please enter two different numbers (Integers)</h2>
                Enter Number 1 -->   <input type="text" placeholder="Enter Only Numbers..."
                                            onChange={event => setNumber1(event.target.value)}/>
                <br/>
                Enter Number 2 -->  <input type="text" placeholder="Enter Only Numbers..."
                                           onChange={event => setNumber2(event.target.value)}/>
                <br/>
                <br/>
                <button onClick={generateNewValueOnClick}>Click to generate a new random value</button>
                <div>
                    <br/>
                    <span className={'randNum'}>The Random Number is --> {randNum}</span>

                </div>
            </center>
        </div>
    );
}

export default App;


