import random


from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from my_server.serializers import RandomValueSerializer
from my_server.models import RandomValue


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def generate_new_value(request):

    num_from_user1 = request.POST.get('number1', '')
    num_from_user2 = request.POST.get('number2', '')

    # get 2 numbers from request
    # get random on 2 numbers (range)
    # return the new number as response

    random_value = get_random_value(num_from_user1, num_from_user2)
    new_value = persist_new_value(random_value)
    serializer = RandomValueSerializer(new_value)
    return Response(serializer.data, status=status.HTTP_200_OK)


def persist_new_value(random_value):
    new_value = RandomValue(value=random_value)
    new_value.save()
    return new_value

def get_random_value(number1, number2):
    if number1 > number2:
        random_value = random.randint(number2, number1)
    else:
        random_value = random.randint(number1, number2)

    while .Objects.filter(value=random_value)!=None
        random_value = random.randint(number1, number2)

    return random_value