from django.db import models


class RandomValue(models.Model):
    id = models.AutoField(primary_key=True)
    value = models.IntegerField(unique=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%d' % self.value

